﻿using System;

namespace Test_v2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            TestCaseIsContain("Test Case 1", new int[] { 90, 22, 10, 33 }, new int[] { 34, 90, 22, 10, 33 }, "True");
            TestCaseIsContain("Test Case 2", new int[] { 90, 10, 33 }, new int[] { 34, 90, 22, 10, 33 }, "False");
            TestCaseIsContain("Test Case 3", new int[] { 22, 10 }, new int[] { 34, 90, 22, 10, 33 }, "True");
            TestCaseIsContain("Test Case 4", new int[] { }, new int[] { 34, 90, 22, 10, 33 }, "False");
            TestCaseIsContain("Test Case 5", new int[] { }, new int[] { }, "False");
            TestCaseIsContain("Test Case 6", new int[] { 22, 10 }, new int[] { 22, 22, 10 }, "True");
            TestCaseIsContain("Test Case 7", new int[] { 34, 90, 22, 10, 33 }, new int[] { 22, 10 }, "True");
        }

        private static void TestCaseIsContain(string testName, int[] arrayA, int[] arrayB, string expectResult)
        {
            Console.WriteLine(testName);
            var result = IsContain(arrayA, arrayB);
            Console.WriteLine($"IsContain({ConvertIntArrayToString(arrayA)},{ConvertIntArrayToString(arrayB)}) Expect {expectResult} // {result}");
        }

        private static string ConvertIntArrayToString(int[] arrayInt) => $"[{string.Join(',', arrayInt)}]";

        //private static string ConvertIntArrayToString(int[] arrayInt)
        //{
        //    return $"[{string.Join(',', arrayInt)}]";
        //}

        private static bool IsContain(int[] arrayA, int[] arrayB)
        {
            if (arrayB.Length == 0 || arrayA.Length == 0)
            {
                return false;
            }

            int countCorrect = 0;
            for (int i = 0; i < arrayB.Length; i++)
            {
                if (countCorrect < arrayA.Length)
                {
                    if (arrayA[countCorrect] == arrayB[i])
                    {
                        countCorrect++;
                    }
                    else
                    {
                        countCorrect = 0;
                        if (arrayA[countCorrect] == arrayB[i])
                        {
                            countCorrect++;
                        }
                    }
                }
                else
                {
                    //Check is all check
                    return true;
                }
            }
            return countCorrect == arrayA.Length;
        }

        private static bool IsContainRecur(int[] arrayA, int[] arrayB, int indexCount = 0, int countCorrect = 0)
        {
            if (arrayB.Length == 0 || arrayA.Length == 0)
            {
                return false;
            }

            indexCount++;

            for (int i = 0; i < arrayB.Length; i++)
            {
                if (countCorrect < arrayA.Length)
                {
                    if (arrayA[countCorrect] == arrayB[i])
                    {
                        countCorrect++;
                    }
                    else
                    {
                        countCorrect = 0;
                        if (arrayA[countCorrect] == arrayB[i])
                        {
                            countCorrect++;
                        }
                    }
                }
            }
            return countCorrect == arrayA.Length;
        }
    }
}